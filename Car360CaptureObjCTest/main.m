//
//  main.m
//  Car360CaptureObjCTest
//
//  Created by Reed Carson on 6/4/18.
//  Copyright © 2018 Carvana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
