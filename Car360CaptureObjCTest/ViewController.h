//
//  ViewController.h
//  Car360Captire-objc-testp
//
//  Created by Reed Carson on 6/1/18.
//  Copyright © 2018 Carvana. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Car360Framework;

@interface ViewController : UIViewController <CaptureViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>


@end

