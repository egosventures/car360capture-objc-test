//
//  ViewController.m
//  Car360Captire-objc-testp
//
//  Created by Reed Carson on 6/1/18.
//  Copyright © 2018 Carvana. All rights reserved.
//

#import "ViewController.h"

@import Car360Framework;

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)openCapture:(id)sender;

@property NSMutableArray<NSString *> *stableIds;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableview.delegate = self;
    _tableview.dataSource = self;
}


- (void)viewWillAppear:(BOOL)animated {
    if (![Car360Framework frameworkActive]) {
        [Car360Framework initFrameworkWithUsername:@"dgpratt4@gmail.com" password:@"password123" completionBlock:^(BOOL activated) {
            if (activated) {
                NSMutableArray *array = [NSMutableArray arrayWithArray:[Car360Framework listLocalStableIds]];
                self.stableIds = array;
                [self.tableview reloadData];
            } else {
                NSLog(@"Activation failed");
            }
        }];
    } else {
        NSMutableArray *array = [NSMutableArray arrayWithArray:[Car360Framework listLocalStableIds]];
        self.stableIds = array;
        [self.tableview reloadData];
        NSLog(@"framework active");
    }
}

- (void)openCapture:(id)sender {
    CaptureViewController *captureVC = [Car360Framework getCaptureViewControllerWithDelegate:(self) defaultCaptureType:(CaptureTypeWalkAround) carIdentificationType:(CarIdTypeVin) carIdentification:@"MY_VIN_1234" carCondition:(CarConditionNew)];
    
    [self presentViewController:captureVC animated:true completion:^{}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)captureViewController:(CaptureViewController * _Nonnull)captureViewController didFinishCapturingWithStableId:(NSString * _Nullable)stableId carIdentificationType:(enum CarIdType)carIdentificationType carIdentification:(NSString * _Nullable)carIdentification spinSaved:(BOOL)spinSaved canceled:(BOOL)canceled {
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *id = _stableIds[indexPath.row];
    ViewerViewController *viewerVC = [Car360Framework getLocalViewerViewControllerWithStableId:id buttonsToShow: (ViewerButtonsBack)];
    
    [self presentViewController:viewerVC animated:true completion:nil];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellId"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellId"];
    }
    
    cell.textLabel.text = [_stableIds objectAtIndex:indexPath.row];
    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.stableIds count];
}



@end
