//
//  AppDelegate.h
//  Car360CaptureObjCTest
//
//  Created by Reed Carson on 6/4/18.
//  Copyright © 2018 Carvana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

